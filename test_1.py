import requests
from routes import make_response
import pytest
import asyncio
import motor.motor_asyncio
from pymongo import MongoClient


DB_URI = 'mongodb://db:27017'
DB = 'test'


@pytest.fixture
async def mongo(event_loop):
    client = motor.motor_asyncio.AsyncIOMotorClient(DB_URI, io_loop=event_loop)
    db = client[DB]
    await db['test'].insert_one({'color': 'red', 'salutation': 'hello'})
    yield
    await db['test'].delete_many({})


@pytest.fixture
def mongo_sync():
    client = MongoClient('mongodb://db:27017/')
    db = client[DB]
    db['test'].insert_one({'color': 'red', 'salutation': 'hello'})
    yield
    db['test'].delete_many({})


@pytest.mark.asyncio
async def test_get_hi(mongo):
    r = requests.get('http://api:8888/hi')
    assert r.json() == {'hello ;) ': 'world'}


def test_get_hi_sync(mongo_sync):
    r = requests.get('http://api:8888/hi')
    assert r.json() == {'hello ;) ': 'world'}


def test_make():
    assert make_response('hello') == 'hello ;) '


def test_post_auth():
    r = requests.post('http://api:8888/auth')
    assert r.json() == {'error': "JWT Decode Error"}


def test_post_auth_success():
    r = requests.post('http://api:8888/auth',
                      headers={'Authorization':
                               'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.'
                               'eyJ1c2VyIjoibWlndWVsIn0.'
                               'XnBRt8Lo5aG0UjWWgPi683fgWi3yBNh4gZVh9YW-9Fg'})
    assert r.json() == {'hello': 'miguel'}
