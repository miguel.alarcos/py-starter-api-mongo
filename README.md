docker-compose up --build

This is a way of programming the API and doing TDD and end-2-end with docker-compose:

The API is made with aiohttp and mongodb, and you don't need to mock the database as there's a instance up.

Imagine that you made an update_one with $inc. Instead of mock and test if the database is called with some argument in the form of {'$inc': {'c': 1}} you test the final data in DB.

It's easier and less prone to error.

Here we are testing against DB:

```python
# file: side_effect.py

async def f(db):
    await db['test'].update_one({'color': 'red'}, {'$inc': {'c': 1}})


# file: test_3.py

import pytest
import motor.motor_asyncio
from side_effect import f


DB_URI = 'mongodb://db:27017'
DB = 'test'


@pytest.fixture
async def db(event_loop):
    client = motor.motor_asyncio.AsyncIOMotorClient(DB_URI, io_loop=event_loop)
    db = client[DB]
    await db['test'].insert_one({'color': 'red', 'c': 0})
    yield db
    await db['test'].delete_many({})


@pytest.mark.asyncio
async def test_f(db):
    await f(db)
    doc = await db['test'].find_one({'color': 'red'})
    assert doc['c'] == 1
```
Y así un end-2-end:

```python
pytest.fixture
def mongo_sync():
    client = MongoClient('mongodb://db:27017/')
    db = client[DB]
    db['test'].insert_one({'color': 'red', 'salutation': 'hello'})
    yield
    db['test'].delete_many({})


def test_get_hi_sync(mongo_sync):
    r = requests.get('http://api:8888/hi')
    assert r.json() == {'hello ;) ': 'world'}
```

Also classic testing:

```python
import sys
import multiprocessing
from unittest.mock import MagicMock
from multip import main, hi
import pytest


@pytest.fixture
def data():
    return 'bober'


def test(monkeypatch, data):
    m = MagicMock()
    monkeypatch.setattr(multiprocessing, 'Process', m)
    main('bob')
    m.assert_called_once_with(target=hi, args=data)
```

And here some useful functions to help you create the API. It's base in the concept of access, that let you create permissions not at document level but field level.

```python
@post(routes, '/api/test', 'test') # routes aiohttp object, path, colección
async def handle_post(doc, user, request):
    validate(instance=doc, schema=some_schema) # validate with jsonschema
    access = (('*', ['b', 'c.a']),) # Todo usuario puede hacer insert en colección test para los campos 'b' y 'c.a' (nested)
    doc = pick(doc, access) # pick extrae los campos de doc acorde al access
    doc['owner'] = user
    return doc # el valor retornado es lo que se guardará en mongo

@get(routes, '/api/auth/test/{_id}', 'test')
async def handle_get(doc, user, request):
    access = (('owner', '*'), ('*', ['x'])) # el usuario en el campo 'owner' del documento con _id puede acceder a todos los campos. Ejemplo, si en mongo tenemos el documento {_id: 0, owner: 'miguel', x: 0, y: 1}, entonces el usuario 'miguel', podrá leer los campos x e y del documento con _id 0. El resto de usuarios pueden leer el campo x
    return pick(doc, access, user)

@put(routes, '/api/auth/budget/{_id}', 'budgets')
async def handle_put(doc, user, old_doc, request):
    access = (('owner', '*'),)  # only the owner of the budget can modify it
    return pick_update(doc, access, user, old_doc)

# at the moment this can not be async
@get_many(routes, '/api/items', 'test')
def handle_get_items(query, user, request):
    docs = yield query # customize query
    access = (('*', '*'),)
    yield pick_list(docs, access, user)

# push to array 'arr'
@push(routes, '/api/auth/push/test/{_id}/arr', 'test', 'arr')
async def handle_push(doc, user, old_doc, request):
    access = (('owner', '*'),)
    return pick_update(doc, access, user, old_doc)

@update_array(routes, '/api/auth/update/test/{_id}/arr/{sub_id}', 'test', 'arr')
async def handle_update_array(doc, user, old_doc, request):
    access = (('owner', '*'),)
    return pick_update(doc, access, user, old_doc)

@pull(routes, '/api/auth/pull/test/{_id}/arr/{sub_id}', 'test', 'arr')
async def handle_pull(doc, user, old_doc, request):
    return old_doc["owner"] == user  # return True or False to restrict pull
```
