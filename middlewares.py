from aiohttp import web
import jwt


class JWTDecodeError(Exception):
    def __init__(self):
        super().__init__("JWT Decode Error")


def db_middleware(db):
    @web.middleware
    async def _db_middleware(request, handler):
        request['db'] = db
        response = await handler(request)
        return response
    return _db_middleware


@web.middleware
async def auth_middleware(request, handler):
    if request.path.startswith('/auth'):
        try:
            payload = jwt.decode(request.headers['Authorization'], 'secret',
                                 algorithms=['HS256'])
        except:
            raise JWTDecodeError()
        request['token'] = payload
    response = await handler(request)
    return response


@web.middleware
async def error_middleware(request, handler):
    try:
        response = await handler(request)
        return response
    except Exception as ex:
        return web.json_response({'error': str(ex)})
