import asyncio
from aiohttp import web
import motor.motor_asyncio
import pytest
import os
import jwt
from middlewares import db_middleware, auth_middleware, error_middleware
from routes import set_routes


DB_URI = os.getenv("DB_URI")
SECRET = os.getenv("SECRET")
DB = 'test'
client = motor.motor_asyncio.AsyncIOMotorClient(DB_URI)
db = client[DB]


async def handle(loop):
    routes = web.RouteTableDef()
    set_routes(routes)
    app = web.Application(loop=loop, middlewares=[error_middleware,
                                                  db_middleware(db),
                                                  auth_middleware])
    app.router.add_routes(routes)
    await loop.create_server(app.make_handler(), '0.0.0.0', 8888)


def main():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(handle(loop))
    print("Server started at port 8888")
    loop.run_forever()
    loop.close()


# def main():
#    app = web.Application()
#    app.add_routes(routes)

#    print('serving at 0.0.0.0:8888')
#    web.run_app(app, host="0.0.0.0", port='8888')
