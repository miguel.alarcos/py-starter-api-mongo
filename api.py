from aiohttp import web
from bson import ObjectId
from flatten_dict import flatten
from access import point_reducer


class DocumentNotFoundError(Exception):
    def __init__(self, _id):
        super().__init__("Document with id " + _id + " not found")


class JSONParseError(Exception):
    def __init__(self):
        super().__init__("Error while parsing JSON")


class ObjectIdError(Exception):
    def __init__(self, _id):
        super().__init__("ObjectId not valid: " + _id)


def get(routes, path, col):
    def decorator(f):
        @routes.get(path)
        async def helper(request):
            db = request['db']
            try:
                user = request['token']['user']
            except:
                user = None
            _id = request.match_info.get('_id')
            try:
                oid = ObjectId(_id)
            except:
                raise ObjectIdError(_id)
            doc = await db[col].find_one({'_id': oid})
            if doc is None:
                raise DocumentNotFoundError(_id)
            ret = await f(doc, user, request)
            ret['_id'] = str(ret['_id'])
            return web.json_response(ret)
        return helper
    return decorator


def get_many(routes, path, col):
    def decorator(f):
        @routes.get(path)
        async def helper(request):
            db = request['db']
            try:
                user = request['token']['user']
            except:
                user = None
            query = request.rel_url.query
            g = f(query, user, request)
            query = next(g)
            cursor = db[col].find(query)
            docs = await cursor.to_list(length=100)
            docs = g.send(docs)
            ret = []
            for doc in docs:
                doc['_id'] = str(doc['_id'])
                ret.append(doc)
            return web.json_response(ret)
        return helper
    return decorator


def post(routes, path, col):
    def decorator(f):
        @routes.post(path)
        async def helper(request):
            db = request['db']
            try:
                user = request['token']['user']
            except:
                user = None
            try:
                doc = await request.json()
            except:
                raise JSONParseError()
            ret = await f(doc, user, request)
            await db[col].insert_one(ret)
            ret['_id'] = str(ret['_id'])
            return web.json_response(ret)
        return helper
    return decorator


def put(routes, path, col):
    def decorator(f):
        @routes.put(path)
        async def helper(request):
            db = request['db']
            try:
                doc = await request.json()
            except:
                raise JSONParseError()
            try:
                user = request['token']['user']
            except:
                user = None
            _id = request.match_info.get('_id')
            try:
                oid = ObjectId(_id)
            except:
                raise ObjectIdError(_id)
            old_doc = await db[col].find_one({'_id': oid})
            if old_doc is None:
                raise DocumentNotFoundError(_id)
            ret = await f(doc, user, old_doc, request)
            if ret != {}:
                flatten_document = flatten(ret, reducer=point_reducer)
                await db[col].update_one({'_id': ObjectId(_id)},
                                         {'$set': flatten_document})
                return web.json_response(ret)
            else:
                return web.json_response({})
        return helper
    return decorator


def push(routes, path, col, field):
    def decorator(f):
        @routes.put(path)
        async def helper(request):
            db = request['db']
            try:
                doc = await request.json()
            except:
                raise JSONParseError()
            try:
                user = request['token']['user']
            except:
                user = None
            _id = request.match_info.get('_id')
            try:
                oid = ObjectId(_id)
            except:
                raise ObjectIdError(_id)
            old_doc = await db[col].find_one({'_id': oid})
            if old_doc is None:
                raise DocumentNotFoundError(_id)
            ret = await f(doc, user, old_doc, request)
            if ret != {}:
                ret['_id'] = ObjectId()
                await db[col].update_one({'_id': ObjectId(_id)},
                                         {'$push': {field: ret}})
                ret['_id'] = str(ret['_id'])
                return web.json_response(ret)
            else:
                return web.json_response({})
        return helper
    return decorator


def pull(routes, path, col, field):
    def decorator(f):
        @routes.put(path)
        async def helper(request):
            db = request['db']
            try:
                user = request['token']['user']
            except:
                user = None
            _id = request.match_info.get('_id')
            sub_id = request.match_info.get('sub_id')
            try:
                oid = ObjectId(_id)
                osub_id = ObjectId(sub_id)
            except:
                raise ObjectIdError(_id)
            old_doc = await db[col].find_one({'_id': oid})
            if old_doc is None:
                raise DocumentNotFoundError(_id)
            flag = await f(None, user, old_doc, request)
            if flag:
                await db[col].update_one({'_id': oid},
                                         {'$pull': {field: {'_id': osub_id}}})
            return web.json_response({})
        return helper
    return decorator


def update_array(routes, path, col, field):
    def decorator(f):
        @routes.put(path)
        async def helper(request):
            db = request['db']
            try:
                doc = await request.json()
            except:
                raise JSONParseError()
            try:
                user = request['token']['user']
            except:
                user = None
            _id = request.match_info.get('_id')
            sub_id = request.match_info.get('sub_id')
            try:
                oid = ObjectId(_id)
                osub_id = ObjectId(sub_id)
            except:
                raise ObjectIdError(_id)
            old_doc = await db[col].find_one({'_id': oid})
            if old_doc is None:
                raise DocumentNotFoundError(_id)
            ret = await f(doc, user, old_doc, request)
            if ret != {}:
                doc_to_set = {field + '.$.' + k: v for k, v in ret.items()}
                await db[col].update_one({'_id': oid, field + '._id': osub_id},
                                         {'$set': doc_to_set})
                # ret['_id'] = str(ret['_id'])
                return web.json_response(ret)
            else:
                return web.json_response({})
        return helper
    return decorator
