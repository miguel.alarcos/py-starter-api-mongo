import requests
import pytest
from access import pick, pick_update
from pymongo import MongoClient
from bson import ObjectId


DB_URI = 'mongodb://db:27017'
DB = 'test'


@pytest.fixture
def db():
    client = MongoClient('mongodb://db:27017/')
    db = client[DB]
    yield db
    db['test'].delete_many({})


@pytest.fixture
def _id():
    client = MongoClient('mongodb://db:27017/')
    db = client[DB]
    doc = {'owner': 'miguel', 'b': 321}
    db['test'].insert_one(doc)
    yield doc['_id']
    db['test'].delete_many({})


@pytest.fixture
def insert_many():
    client = MongoClient('mongodb://db:27017/')
    db = client[DB]
    docs = [{'color': 'red', 'a': 0}, {'color': 'red', 'a': 1}]
    db['test'].insert_many(docs)
    ret = []
    for doc in docs:
        doc['_id'] = str(doc['_id'])
        ret.append(doc)
    yield ret
    db['test'].delete_many({})


def test_all_can_read_all():
    user = 'mike'
    document = {'a': 1, 'b': 2}
    access = (('*', '*'),)
    result = pick(document, access, user)
    assert result == document


def test_user_can_read_all():
    user = 'mike'
    document = {'owner': 'mike', 'a': 1, 'b': 2}
    access = (('owner', '*'),)
    result = pick(document, access, user)
    assert result == document


def test_user_can_read_some():
    user = 'mike'
    document = {'owner': 'mike', 'a': 1, 'b': 2, 'c': {'x': 1, 'y': 2}}
    access = (('owner', ['a', 'c.x']),)
    result = pick(document, access, user)
    assert result == {'a': 1, 'c': {'x': 1}}


def test_other_can_read_some():
    user = 'vero'
    document = {'owner': 'mike', 'a': 1, 'b': 2, 'c': {'x': 1, 'y': 2}}
    access = (('owner', ['a', 'c.x']), ('*', ['b']))
    result = pick(document, access, user)
    assert result == {'b': 2}


def test_can_insert_some():
    document = {'guy': 'mike', 'a': 1, 'b': 2, 'c': {'x': 1, 'y': 2}}
    access = (('*', ['b', 'guy']),)
    result = pick(document, access)
    assert result == {'b': 2, 'guy': 'mike'}


def test_can_update_some():
    user = 'mike'
    old_doc = {'owner': 'mike'}
    document = {'a': 15}
    access = (('owner', ['a']),)
    result = pick_update(document, access, user, old_doc)
    assert result == {'a': 15}


def test_cannot_update():
    user = 'ber'
    old_doc = {'owner': 'mike'}
    document = {'a': 15}
    access = (('owner', ['a']),)
    result = pick_update(document, access, user, old_doc)
    assert result == {}
