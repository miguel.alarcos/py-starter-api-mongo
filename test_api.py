import requests
import pytest
from access import pick, pick_update
from pymongo import MongoClient
from bson import ObjectId


DB_URI = 'mongodb://db:27017'
DB = 'test'


@pytest.fixture
def db():
    client = MongoClient('mongodb://db:27017/')
    db = client[DB]
    yield db
    db['test'].delete_many({})


@pytest.fixture
def _id():
    client = MongoClient('mongodb://db:27017/')
    db = client[DB]
    doc = {'owner': 'miguel', 'b': 321}
    db['test'].insert_one(doc)
    yield doc['_id']
    db['test'].delete_many({})


@pytest.fixture
def _id2():
    client = MongoClient('mongodb://db:27017/')
    db = client[DB]
    doc = {'owner': 'miguel', 'c': {'c2': 0, 'nested': 0}}
    db['test'].insert_one(doc)
    yield doc['_id']
    db['test'].delete_many({})


@pytest.fixture
def arr():
    client = MongoClient('mongodb://db:27017/')
    db = client[DB]
    doc = {'owner': 'miguel', 'arr': [{'_id': ObjectId(), 'x': 'x'}]}
    db['test'].insert_one(doc)
    yield doc
    db['test'].delete_many({})


@pytest.fixture
def insert_many():
    client = MongoClient('mongodb://db:27017/')
    db = client[DB]
    docs = [{'color': 'red', 'a': 0}, {'color': 'red', 'a': 1}]
    db['test'].insert_many(docs)
    ret = []
    for doc in docs:
        doc['_id'] = str(doc['_id'])
        ret.append(doc)
    yield ret
    db['test'].delete_many({})


def test_insert(db):
    r = requests.post('http://api:8888/insert', json={'guy': 'mike',
                                                      'a': 0, 'b': 2})
    r = r.json()
    _id = r.pop('_id')
    assert r == {'guy': 'mike', 'b': 2}
    inserted = db['test'].find_one({'_id': ObjectId(_id)})
    assert inserted == {'_id': ObjectId(_id), 'guy': 'mike', 'b': 2}


def test_get(_id):
    r = requests.get('http://api:8888/auth/get/' + str(_id), headers={
                               'Authorization':
                               'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.'
                               'eyJ1c2VyIjoibWlndWVsIn0.'
                               'XnBRt8Lo5aG0UjWWgPi683fgWi3yBNh4gZVh9YW-9Fg'})
    r = r.json()
    assert r == {'_id': str(_id), 'owner': 'miguel', 'b': 321}


def test_update(_id, db):
    r = requests.put('http://api:8888/auth/update/' + str(_id),
                     json={'b': 333},
                     headers={
                     'Authorization':
                     'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.'
                     'eyJ1c2VyIjoibWlndWVsIn0.'
                     'XnBRt8Lo5aG0UjWWgPi683fgWi3yBNh4gZVh9YW-9Fg'})
    r = r.json()
    assert r == {'b': 333}
    doc = db['test'].find_one({'_id': ObjectId(_id)})
    assert doc == {'_id': ObjectId(_id), 'owner': 'miguel', 'b': 333}


def test_update_nested(_id2, db):
    r = requests.put('http://api:8888/auth/update/' + str(_id2),
                     json={'c': {'nested': 333}},
                     headers={
                     'Authorization':
                     'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.'
                     'eyJ1c2VyIjoibWlndWVsIn0.'
                     'XnBRt8Lo5aG0UjWWgPi683fgWi3yBNh4gZVh9YW-9Fg'})
    r = r.json()
    assert r == {'c': {'nested': 333}}
    doc = db['test'].find_one({'_id': ObjectId(_id2)})
    assert doc == {'_id': ObjectId(_id2), 'owner': 'miguel',
                   'c': {'c2': 0, 'nested': 333}}


def test_update_fail_json(_id, db):
    r = requests.put('http://api:8888/auth/update/' + str(_id),
                     # json={'b': 333},
                     headers={
                     'Authorization':
                     'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.'
                     'eyJ1c2VyIjoibWlndWVsIn0.'
                     'XnBRt8Lo5aG0UjWWgPi683fgWi3yBNh4gZVh9YW-9Fg'})
    r = r.json()
    assert r == {"error": "Error while parsing JSON"}


def test_update_fail_auth_header(_id, db):
    r = requests.put('http://api:8888/auth/update/' + str(_id),
                     json={'b': 333},
                     headers={
                     'Authorization':
                     'xxx'})
    r = r.json()
    assert r == {"error": "JWT Decode Error"}


def test_get_many(insert_many):
    r = requests.get('http://api:8888/items?color=red')
    r = r.json()
    assert r == insert_many


def test_push(_id, db):
    r = requests.put('http://api:8888/auth/push/' + str(_id),
                     json={'b': 333},
                     headers={
                     'Authorization':
                     'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.'
                     'eyJ1c2VyIjoibWlndWVsIn0.'
                     'XnBRt8Lo5aG0UjWWgPi683fgWi3yBNh4gZVh9YW-9Fg'})
    r = r.json()
    sub_id = r.pop('_id')
    assert r == {'b': 333}
    doc = db['test'].find_one({'_id': ObjectId(_id)})
    assert doc == {'_id': ObjectId(_id), 'owner': 'miguel', 'b': 321,
                   'arr': [{'_id': ObjectId(sub_id), 'b': 333}]}


def test_push_document_not_found():
    _id = str(ObjectId())
    r = requests.put('http://api:8888/auth/push/' + _id,
                     json={'b': 333},
                     headers={
                      'Authorization':
                      'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.'
                      'eyJ1c2VyIjoibWlndWVsIn0.'
                      'XnBRt8Lo5aG0UjWWgPi683fgWi3yBNh4gZVh9YW-9Fg'})
    r = r.json()
    assert r == {'error': "Document with id " + _id + " not found"}


def test_push_document_object_id_malformed():
    _id = "xxx"
    r = requests.put('http://api:8888/auth/push/' + _id,
                     json={'b': 333},
                     headers={
                      'Authorization':
                      'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.'
                      'eyJ1c2VyIjoibWlndWVsIn0.'
                      'XnBRt8Lo5aG0UjWWgPi683fgWi3yBNh4gZVh9YW-9Fg'})
    r = r.json()
    assert r == {'error': "ObjectId not valid: " + _id}


def test_update_array(arr, db):
    _id = str(arr['_id'])
    sub_id = str(arr['arr'][0]['_id'])
    r = requests.put('http://api:8888/auth/update-array/' +
                     _id + '/' + sub_id,
                     json={'b': 333},
                     headers={
                      'Authorization':
                      'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.'
                      'eyJ1c2VyIjoibWlndWVsIn0.'
                      'XnBRt8Lo5aG0UjWWgPi683fgWi3yBNh4gZVh9YW-9Fg'})
    r = r.json()
    # sub_id = r.pop('_id')
    assert r == {'b': 333}
    doc = db['test'].find_one({'_id': ObjectId(_id)})
    assert doc == {'_id': ObjectId(_id), 'owner': 'miguel',
                   'arr': [{'_id': ObjectId(sub_id), 'x': 'x', 'b': 333}]}


def test_pull_array(arr, db):
    _id = str(arr['_id'])
    sub_id = str(arr['arr'][0]['_id'])
    r = requests.put('http://api:8888/auth/pull/' +
                     _id + '/' + sub_id,
                     headers={
                      'Authorization':
                      'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.'
                      'eyJ1c2VyIjoibWlndWVsIn0.'
                      'XnBRt8Lo5aG0UjWWgPi683fgWi3yBNh4gZVh9YW-9Fg'})
    r = r.json()
    assert r == {}
    doc = db['test'].find_one({'_id': ObjectId(_id)})
    assert doc == {'_id': ObjectId(_id), 'owner': 'miguel', 'arr': []}
