from flatten_dict import flatten, unflatten


def point_reducer(k1, k2):
    if k1 is None:
        return k2
    else:
        return k1 + "." + k2


def point_splitter(flat_key):
    return flat_key.split(".")


def pick(document, access, user=None):
    for u, fields in access:
        if u == '*' or user == document[u]:
            if fields == '*':
                return document
            else:
                flatten_document = flatten(document, reducer=point_reducer)
                ret = {k: flatten_document[k] for k in fields
                       if k in flatten_document}
                return unflatten(ret, splitter=point_splitter)
    return {}


def pick_update(document, access, user, old_doc):
    for u, fields in access:
        if u == '*' or user == old_doc[u]:
            if fields == '*':
                return document
            else:
                flatten_document = flatten(document, reducer=point_reducer)
                ret = {k: flatten_document[k] for k in fields
                       if k in flatten_document}
                return unflatten(ret, splitter=point_splitter)
    return {}


def pick_list(documents, access, user):
    return [pick(doc, access, user) for doc in documents]
