import sys
import multiprocessing
from unittest.mock import MagicMock
from multip import main, hi
import pytest


@pytest.fixture
def data():
    return 'bober'


def test(monkeypatch, data):
    m = MagicMock()
    monkeypatch.setattr(multiprocessing, 'Process', m)
    main('bob')
    m.assert_called_once_with(target=hi, args=data)


def test_2(monkeypatch, data):
    m = MagicMock()
    monkeypatch.setattr(sys.stdout, 'write', m)
    hi(data)
    m.assert_called_once_with('hello ' + data)
