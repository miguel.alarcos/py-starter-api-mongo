import pytest
import motor.motor_asyncio
from side_effect import f


DB_URI = 'mongodb://db:27017'
DB = 'test'


@pytest.fixture
async def db(event_loop):
    client = motor.motor_asyncio.AsyncIOMotorClient(DB_URI, io_loop=event_loop)
    db = client[DB]
    await db['test'].insert_one({'color': 'red', 'c': 0})
    yield db
    await db['test'].delete_many({})


@pytest.mark.asyncio
async def test_f(db):
    await f(db)
    doc = await db['test'].find_one({'color': 'red'})
    assert doc['c'] == 1
