import sys
import multiprocessing

pr = multiprocessing.Process


def hi(name: str) -> None:
    sys.stdout.write('hello ' + name)


def main(data):
    name = data + 'er'
    p = multiprocessing.Process(target=hi, args=name)
    p.start()
    p.join()
