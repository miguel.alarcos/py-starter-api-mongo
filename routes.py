from aiohttp import web
from access import pick, pick_update, pick_list
from bson import ObjectId
from api import get, get_many, push, put, post, update_array, pull


def make_response(txt: str) -> str:
    return txt + ' ;) '


def set_routes(routes):

    @get_many(routes, '/items', 'test')
    def handle_get_items(query, user, request):
        docs = yield query
        access = (('*', '*'),)
        yield pick_list(docs, access, user)

    @post(routes, '/insert', 'test')
    async def handle_post(doc, user, request):
        access = (('*', ['b', 'guy']),)
        return pick(doc, access)

    @get(routes, '/auth/get/{_id}', 'test')
    async def handle_get_auth(doc, user, request):
        access = (('owner', '*'),)
        return pick(doc, access, user)

    @put(routes, '/auth/update/{_id}', 'test')
    async def handle_put(doc, user, old_doc, request):
        access = (('owner', '*'),)
        return pick_update(doc, access, user, old_doc)

    @push(routes, '/auth/push/{_id}', 'test', 'arr')
    async def handle_push(doc, user, old_doc, request):
        access = (('owner', '*'),)
        return pick_update(doc, access, user, old_doc)

    @update_array(routes, '/auth/update-array/{_id}/{sub_id}', 'test', 'arr')
    async def handle_update_array(doc, user, old_doc, request):
        access = (('owner', '*'),)
        return pick_update(doc, access, user, old_doc)

    @pull(routes, '/auth/pull/{_id}/{sub_id}', 'test', 'arr')
    async def handle_pull(doc, user, old_doc, request):
        return True

    @routes.get('/hi')
    async def handle_get(request):
        db = request['db']
        doc = await db['test'].find_one({'color': 'red'})
        salutation = doc['salutation']
        data = {make_response(salutation): 'world'}
        return web.json_response(data)

    @routes.post('/auth')
    async def handle_post(request):
        return web.json_response({'hello': request['token']['user']})
